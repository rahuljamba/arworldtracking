//
//  Model.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 10/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class LoginModel:NSObject{
    enum Keys: String,CodingKey{
        
        case message = "message"
        case data = "data"
       
    }
    
    var message = ""
    var data = DataModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let message = dictionary[Keys.message.stringValue] as? String{
            self.message = message
        }
        if let data = dictionary[Keys.data.stringValue] as? [String: AnyObject]{
            self.data = DataModel(dictionary: data)
        }
        
        super.init()
    }
    
}


class DataModel:NSObject{
    enum Keys: String,CodingKey{
        
        case passcode = "passcode"
        case profile = "profile"
        
    }
    
    var passcode = ""
    var profile = ProfileModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let passcode = dictionary[Keys.passcode.stringValue] as? String{
            self.passcode = passcode
        }
        if let profile = dictionary[Keys.profile.stringValue] as? [String: AnyObject]{
            self.profile = ProfileModel(dictionary: profile)
        }
        
        super.init()
    }
    
}


class ValidateModel:NSObject{
    enum Keys: String,CodingKey{
        
        case message = "message"
        case data = "data"
        case auth_token = "auth_token"
       
    }
    
    var message = ""
    var data = DataModel()
    var auth_token = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let message = dictionary[Keys.message.stringValue] as? String{
            self.message = message
        }
        if let data = dictionary[Keys.data.stringValue] as? [String: AnyObject]{
            self.data = DataModel(dictionary: data)
        }
        if let auth_token = dictionary[Keys.auth_token.stringValue] as? String{
            self.auth_token = auth_token
        }
        
        super.init()
    }
    
}


class ProfileModel:NSObject{
    enum Keys: String,CodingKey{
        
        case message = "message"
        case data = "data"

    }
    
    var message = ""
    var data = DataProfileModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        super.init()
        //user_detail
        if let message = dictionary[Keys.message.stringValue] as? String{
            self.message = message
        }
        if let data = dictionary[Keys.data.stringValue] as? [String: AnyObject]{
            self.data = DataProfileModel(dictionary: data)
        }
    }
    
}

class DataProfileModel:NSObject{
    enum Keys: String,CodingKey{

        case active_status = "active_status"
        case address = "address"
        case city_id = "city_id"
        case client_id = "client_id"
        case company = "company"
        case created_at = "created_at"
        case created_by = "created_by"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case gender = "gender"
        case id = "id"
        case is_active = "is_active"
        case name = "name"
        case passcode = "passcode"
        case phone = "phone"
        case phone_verified_at = "phone_verified_at"
        case pincode = "pincode"
        case profile_pic = "profile_pic"
        case updated_at = "updated_at"
        case roles = "roles"
        case city = "city"
        case client = "client"
        case created_user = "created_user"
       
    }

    var active_status = ""
    var address = ""
    var city_id = ""
    var client_id = ""
    var company = ""
    var created_at = ""
    var created_by = ""
    var email = ""
    var email_verified_at = ""
    var gender = ""
    var id:Int = 0
    var is_active = ""
    var name = ""
    var passcode = ""
    var phone = ""
    var phone_verified_at = ""
    var pincode = ""
    var profile_pic = ""
    var updated_at = ""
    var roles = [RolesModel]()
    var city = CityModel()
    var client = ClientModel()
    var created_user = CreateUserModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        
        if let active_status = dictionary[Keys.active_status.stringValue] as? String{
            self.active_status = active_status
        }
        if let address = dictionary[Keys.address.stringValue] as? String{
            self.address = address
        }
        if let city_id = dictionary[Keys.city_id.stringValue] as? String{
            self.city_id = city_id
        }
        if let client_id = dictionary[Keys.client_id.stringValue] as? String{
            self.client_id = client_id
        }
        if let company = dictionary[Keys.company.stringValue] as? String{
            self.company = company
        }
        if let created_at = dictionary[Keys.created_at.stringValue] as? String{
            self.created_at = created_at
        }
        if let created_by = dictionary[Keys.created_by.stringValue] as? String{
            self.created_by = created_by
        }
        if let email = dictionary[Keys.email.stringValue] as? String{
            self.email = email
        }
        if let email_verified_at = dictionary[Keys.email_verified_at.stringValue] as? String{
            self.email_verified_at = email_verified_at
        }
        if let gender = dictionary[Keys.gender.stringValue] as? String{
            self.gender = gender
        }
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let is_active = dictionary[Keys.is_active.stringValue] as? String{
            self.is_active = is_active
        }
        if let name = dictionary[Keys.name.stringValue] as? String{
            self.name = name
        }
        if let passcode = dictionary[Keys.passcode.stringValue] as? String{
            self.passcode = passcode
        }
        if let phone = dictionary[Keys.phone.stringValue] as? String{
            self.phone = phone
        }
        if let phone_verified_at = dictionary[Keys.phone_verified_at.stringValue] as? String{
            self.phone_verified_at = phone_verified_at
        }
        if let pincode = dictionary[Keys.pincode.stringValue] as? String{
            self.pincode = pincode
        }
        if let profile_pic = dictionary[Keys.profile_pic.stringValue] as? String{
            self.profile_pic = profile_pic
        }
        if let updated_at = dictionary[Keys.updated_at.stringValue] as? String{
            self.updated_at = updated_at
        }
        if let roles = dictionary[Keys.roles.stringValue] as?
            
            Array<Dictionary<String,AnyObject>>{
            for data in roles {
                let tempData = RolesModel(dictionary: data)
                self.roles.append(tempData)
            }
        }
        if let city = dictionary[Keys.city.stringValue] as? [String: AnyObject]{
            self.city = CityModel(dictionary: city)
        }
        if let client = dictionary[Keys.client.stringValue] as? [String: AnyObject]{
            self.client = ClientModel(dictionary: client)
        }
        if let created_user = dictionary[Keys.created_user.stringValue] as? [String: AnyObject]{
            self.created_user = CreateUserModel(dictionary: created_user)
        }
        super.init()
    }
    
}

class CreateUserModel:NSObject{
    enum Keys: String,CodingKey{
        
        case id = "id"
        case name = "name"
        case active_status = "active_status"
    }
    
    var id:Int = 0
    var name = ""
    var active_status = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let name = dictionary[Keys.name.stringValue] as? String{
            self.name = name
        }
        if let active_status = dictionary[Keys.active_status.stringValue] as? String{
            self.active_status = active_status
        }
        
        super.init()
    }
    
}
class ClientModel:NSObject{
    enum Keys: String,CodingKey{
        
        case id = "id"
        case name = "name"
        case active_status = "active_status"
    }
    
    var id:Int = 0
    var name = ""
    var active_status = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let name = dictionary[Keys.name.stringValue] as? String{
            self.name = name
        }
        if let active_status = dictionary[Keys.active_status.stringValue] as? String{
            self.active_status = active_status
        }
        
        super.init()
    }
    
}
class CityModel:NSObject{
    enum Keys: String,CodingKey{
        
        case id = "id"
        case name = "name"
        case state_id = "state_id"
        case state = "state"
        

    }
    
    var id:Int = 0
    var name = ""
    var state_id = ""
    var state = StateModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let name = dictionary[Keys.name.stringValue] as? String{
            self.name = name
        }
        if let state_id = dictionary[Keys.state_id.stringValue] as? String{
            self.state_id = state_id
        }
        if let state = dictionary[Keys.state.stringValue] as? [String: AnyObject]{
            self.state = StateModel(dictionary: state)
        }
        
        super.init()
    }
    
}


class StateModel:NSObject{
    enum Keys: String,CodingKey{
        
        case id = "id"
        case name = "name"
        case country_id = "country_id"
        case country = "country"
        

    }
    
    var id:Int = 0
    var name = ""
    var country_id = ""
    var country = CountryModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let name = dictionary[Keys.name.stringValue] as? String{
            self.name = name
        }
        if let country_id = dictionary[Keys.country_id.stringValue] as? String{
            self.country_id = country_id
        }
        if let country = dictionary[Keys.country.stringValue] as? [String: AnyObject]{
            self.country = CountryModel(dictionary: country)
        }
        
        super.init()
    }
    
}

class CountryModel:NSObject{
    enum Keys: String,CodingKey{
        
        case id = "id"
        case name = "name"
    }
    
    var id:Int = 0
    var name = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let name = dictionary[Keys.name.stringValue] as? String{
            self.name = name
        }
        
        super.init()
    }
    
}

class RolesModel:NSObject{
    enum Keys: String,CodingKey{
        
        case id = "id"
        case name = "name"
        case pivot = "pivot"

    }
    
    var id:Int = 0
    var name = ""
    var pivot = PivotModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let name = dictionary[Keys.name.stringValue] as? String{
            self.name = name
        }
        if let pivot = dictionary[Keys.pivot.stringValue] as? [String: AnyObject]{
            self.pivot = PivotModel(dictionary: pivot)
        }
        
        super.init()
    }
    
}

class PivotModel:NSObject{
    enum Keys: String,CodingKey{
        
        case model_id = "model_id"
        case model_type = "model_type"
        case role_id = "role_id"

    }
    
    var model_id = ""
    var model_type = ""
    var role_id = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let model_id = dictionary[Keys.model_id.stringValue] as? String{
            self.model_id = model_id
        }
        if let model_type = dictionary[Keys.model_type.stringValue] as? String{
            self.model_type = model_type
        }
        if let role_id = dictionary[Keys.role_id.stringValue] as? String{
            self.role_id = role_id
        }
        super.init()
    }
    
}

class LogoutModel:NSObject{
    enum Keys: String,CodingKey{
        
        case message = "message"
    }
    
    var message = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let message = dictionary[Keys.message.stringValue] as? String{
            self.message = message
        }
        super.init()
    }
    
}


class StoreObjectLocationModel:NSObject{
    enum Keys: String,CodingKey{
        
        case message = "message"
        case data = "data"
       
    }
    
    var message = ""
    var data = DataStoreLocationModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let message = dictionary[Keys.message.stringValue] as? String{
            self.message = message
        }
        if let data = dictionary[Keys.data.stringValue] as? [String: AnyObject]{
            self.data = DataStoreLocationModel(dictionary: data)
        }
        
        super.init()
    }
    
}


class DataStoreLocationModel:NSObject{
    enum Keys: String,CodingKey{
        
        case code = "code"
        case created_at = "created_at"
        case description = "description"
        case file = "file"
        case id = "id"
        case location_id = "location_id"
        case name = "name"
        case ballName = "ballName"
        case updated_at = "updated_at"
        case x_coordinate = "x_coordinate"
        case y_coordinate = "y_coordinate"
        case z_coordinate = "z_coordinate"
       
    }
    
    var code = ""
    var created_at = ""
    var descriptionAr = ""
    var file = ""
    var id:Int = 0
    var location_id:Int = 0
    var name = ""
    var data = ""
    var updated_at = ""
    var x_coordinate = ""
    var y_coordinate = ""
    var z_coordinate = ""
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let code = dictionary[Keys.code.stringValue] as? String{
            self.code = code
        }
        if let created_at = dictionary[Keys.created_at.stringValue] as? String{
            self.created_at = created_at
        }
        if let descriptionAr = dictionary[Keys.description.stringValue] as? String{
            self.descriptionAr = descriptionAr
        }
        if let id = dictionary[Keys.id.stringValue] as? Int{
            self.id = id
        }
        if let location_id = dictionary[Keys.location_id.stringValue] as? Int{
            self.location_id = location_id
        }
        if let updated_at = dictionary[Keys.updated_at.stringValue] as? String{
            self.updated_at = updated_at
        }
        if let x_coordinate = dictionary[Keys.x_coordinate.stringValue] as? String{
            self.x_coordinate = x_coordinate
        }
        if let y_coordinate = dictionary[Keys.y_coordinate.stringValue] as? String{
            self.y_coordinate = y_coordinate
        }
        if let z_coordinate = dictionary[Keys.z_coordinate.stringValue] as? String{
            self.z_coordinate = z_coordinate
        }
        super.init()
    }
    
}


class FatchObjectLocationModel:NSObject{
    enum Keys: String,CodingKey{
        
        case message = "message"
        case data = "data"
       
    }
    
    var message = ""
    var data = [DataFatchLocationModel]()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String: JSON]) {
        
        //user_detail
        if let message = dictionary[Keys.message.stringValue] as? String{
            self.message = message
        }
        if let data = dictionary[Keys.data.stringValue] as? [String: JSON]{
            let object = DataFatchLocationModel(dictionary: data)
            self.data.append(object)
        }
        
        
        super.init()
    }
    
}


class DataFatchLocationModel:NSObject{
    enum Keys: String,CodingKey{
        
        case code = "code"
        case created_at = "created_at"
        case description = "description"
        case ballName = "ballName"
        case file = "file"
        case id = "id"
        case location_id = "location_id"
        case name = "name"
        case x_coordinate = "x_coordinate"
        case y_coordinate = "y_coordinate"
        case z_coordinate = "z_coordinate"
       
    }
    
    var code = ""
    var created_at = ""
    var descriptionAr = ""
    var ballName = ""
    var file = ""
    var id:Int = 0
    var location_id:Int = 0
    var name = ""
    var data = ""
    var x_coordinate = ""
    var y_coordinate = ""
    var z_coordinate = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:JSON]) {
        
        //user_detail
        self.code = dictionary[Keys.code.stringValue]?.stringValue ?? ""
        self.created_at = dictionary[Keys.created_at.stringValue]?.stringValue ?? ""
        self.ballName = dictionary[Keys.ballName.stringValue]?.stringValue ?? ""
        self.name = dictionary[Keys.name.stringValue]?.stringValue ?? ""
        self.descriptionAr = dictionary[Keys.description.stringValue]?.stringValue ?? ""
        self.id = dictionary[Keys.id.stringValue]?.intValue ?? 0
        self.location_id = dictionary[Keys.location_id.stringValue]?.intValue ?? 0
        self.x_coordinate = dictionary[Keys.x_coordinate.stringValue]?.stringValue ?? ""
        self.y_coordinate = dictionary[Keys.y_coordinate.stringValue]?.stringValue ?? ""
        self.z_coordinate = dictionary[Keys.z_coordinate.stringValue]?.stringValue ?? ""
        
        super.init()
    }
    
}

//
//class TableListModel{
//    enum Keys: String,CodingKey{
//
//        case message = "message"
//        case data = "data"
//
//    }
//
//    var message = ""
//    var data = [DataListModel]()
//
//    init(dictionary:[String:Any]) {
//
//        //user_detail
//        if let message = dictionary[Keys.message.stringValue] as? String{
//            self.message = message
//        }
//        if let data = dictionary[Keys.data.stringValue] as? Array<Any>{
//            for dataDict in data {
//                let tempData = DataListModel(dictionary: dataDict as! [String : Any])
//                self.data.append(tempData)
//            }
//        }
//    }
//
//}


class DataListModel:NSObject{
    enum Keys: String,CodingKey{
        case active_status = "active_status"
        case city_id = "city_id"
        case client_id = "client_id"
        case created_at = "created_at"
        case floor = "floor"
        case id = "id"
        case is_active = "is_active"
        case image = "image"
        case name = "name"
        case updated_at = "updated_at"
        case city = "city"
        case client = "client"
        case pivot = "pivot"
       
    }
    var active_status = ""
    var city_id = ""
    var client_id = ""
    var created_at = ""
    var floor = ""
    var id:Int = 0
    var is_active = ""
    var image = ""
    var name = ""
    var updated_at = ""
    var city = CityModel()
    var client = ClientModel()
    var pivot = PivotListModel()
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String: JSON]) {
        
        super.init()
        let active_status = dictionary[Keys.active_status.stringValue]?.stringValue
        self.active_status = active_status ?? ""
    
        self.city_id = dictionary[Keys.city_id.stringValue]?.stringValue ?? ""
        self.client_id = dictionary[Keys.client_id.stringValue]?.stringValue ?? ""
        self.created_at = dictionary[Keys.created_at.stringValue]?.stringValue ?? ""
        self.floor = dictionary[Keys.floor.stringValue]?.stringValue ?? ""
        self.id = dictionary[Keys.id.stringValue]?.intValue ?? 0
        self.is_active = dictionary[Keys.is_active.stringValue]?.stringValue ?? ""
        self.name = dictionary[Keys.name.stringValue]?.stringValue ?? ""
        self.image = dictionary[Keys.image.stringValue]?.stringValue ?? ""
        self.updated_at = dictionary[Keys.updated_at.stringValue]?.stringValue ?? ""
      
        if let city = dictionary[Keys.city.stringValue] as? [String: AnyObject]{
            self.city = CityModel(dictionary: city)
        }
        if let client = dictionary[Keys.client.stringValue] as? [String: AnyObject]{
            self.client = ClientModel(dictionary: client)
        }
        if let pivot = dictionary[Keys.pivot.stringValue] as? [String: AnyObject]{
            self.pivot = PivotListModel(dictionary: pivot)
        }
    }
    
}


class PivotListModel:NSObject{
    enum Keys: String,CodingKey{
        
        case location_id = "location_id"
        case user_id = "user_id"
       
    }
    
    var location_id = ""
    var user_id = ""
    
    override init() {
        
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        //user_detail
        if let location_id = dictionary[Keys.location_id.stringValue] as? String{
            self.location_id = location_id
        }
        if let user_id = dictionary[Keys.user_id.stringValue] as? String{
            self.user_id = user_id
        }
        super.init()
    }
    
}
