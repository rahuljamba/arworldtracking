//
//  LoginVC.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 06/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import UIKit


class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var getPassCodeBtn: UIButton!
    @IBOutlet weak var gmailTextfield: UITextField!
    var loginData = LoginModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        gmailTextfield.delegate = self
        gmailTextfield.returnKeyType = .done
        
        getPassCodeBtn.layer.cornerRadius = 10.0
        
        gmailTextfield.layer.cornerRadius = 10.0
        gmailTextfield.clipsToBounds = true
        
        gmailTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        gmailTextfield.layer.borderWidth = 1.0
    }
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        
        if gmailTextfield.text != ""{
            LoginAPICall(email: gmailTextfield.text!)
          //  worldMapGlobalKey = "1"
        }
        
    }
    
    func LoginAPICall(email: String){
        CommonClass.showLoader()
        Webservices.sharedInstance.LoginWebService(email:email){(result, response,message) in
            CommonClass.hideLoader()
            if getStatusCode == 200{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ValidateOTPVC") as! ValidateOTPVC
                vc.emailStr = self.gmailTextfield.text!
                self.navigationController?.pushViewController(vc, animated: true)
            
            }
            else if getStatusCode == 422{
                self.showAlert(withTitle: "EFridge", withMessage: "The selected email is invalid")
            }
            else if getStatusCode == 401{
                appDelegate.logoutMethod()
            }
            else{
                if let somecotegory = response{
                    
                self.loginData = somecotegory
                }else {
                
            }

        }
     }
  }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        gmailTextfield.resignFirstResponder()
        return true
    }
    
}

extension  UIViewController {

    func showAlert(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
//        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
//        })
        alert.addAction(ok)
       // alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
}
