
//  ViewController.swift
//  ARKitWorkingWithWorldMapData

import UIKit
import ARKit
import QuartzCore

class ViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate {

    @IBOutlet weak var heightConstraint: NSLayoutConstraint?

    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var label: UILabel!

    @IBOutlet weak var labelTopic: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var viewDis: UIView!
    @IBOutlet weak var saveViewBtn: UIButton!

    @IBOutlet weak var saveExperienceButton: UIButton!
    @IBOutlet weak var loadExperienceButton: UIButton!

    var getBallName =  String()
    var ballDictArray = NSArray()
    var fetchArray = [FetchedBallData]()
    var selectRoom:DataListModel!
    var sphereNode : SCNNode!
    var shhreNodeArry: [SCNNode] = []
    var cameraNode = SCNNode()
    var StoreLocationData = StoreObjectLocationModel()
    var FatchLocationData = [DataFatchLocationModel]()
    var ismanuallyAdded:Bool = false

    struct FetchedBallData {
        var topic: String
        var desc: String
        var ballName: String
        var ballid : Int = 0
    }

    lazy var worldMapURL: URL = {

        let worldMapString = "worldMapURL\(selectRoom.id)"

        do {
            return try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent(worldMapString)
        } catch {
            fatalError("Error getting world map URL from document directory.")
        }
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        sceneView.delegate = self

        configureLighting()
        addTapGestureToSceneView()
        // StoreLocationAPICall(email: "user@demo.com", location_id: 1, name: "Laptop")
        FatchLocationAPICall(location_id: selectRoom.id)
    }

    func setUI() {

        labelTopic.delegate = self
        labelTopic.returnKeyType = .done

        textView.delegate = self
        textView.returnKeyType = .done

        labelTopic.layer.cornerRadius = 10.0

        labelTopic.clipsToBounds = true
        labelTopic.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        labelTopic.layer.borderWidth = 1.0

        textView.layer.cornerRadius = 10.0

        textView.clipsToBounds = true
        textView.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        textView.layer.borderWidth = 1.0

        view.layer.cornerRadius = 10.0
        saveViewBtn.layer.cornerRadius = 10.0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        labelTopic.resignFirstResponder()
        textView.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        heightConstraint?.constant = UIScreen.main.bounds.height - (UIScreen.main.bounds.height/2)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)  {
        heightConstraint?.constant = 0
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        heightConstraint?.constant = UIScreen.main.bounds.height - (UIScreen.main.bounds.height/2)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        heightConstraint?.constant = 0
    }

    func addTapGestureToSceneView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didReceiveTapGesture(_:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func didReceiveTapGesture(_ sender: UITapGestureRecognizer) {
        if viewDis.isHidden {
            let location = sender.location(in: sceneView)
            guard let hitTestResult = sceneView.hitTest(location, types: [.featurePoint, .estimatedHorizontalPlane] ).first
            else { return }


            var nodeAvailable = false
            let results = sceneView.hitTest(location, options: [SCNHitTestOption.searchMode : 1])
            guard sender.state == .ended else { return }
            for result in results.filter( { $0.node.name != nil }) {
                // if result.node.name == "sphereNODE"
                if shhreNodeArry.contains(result.node){
                    print(result.node.name)
                    //assign selected node to global variable
                    sphereNode = result.node
                    for dicData in self.fetchArray {
                        if result.node.name == (dicData.ballName) as String {
                            textView.text = (dicData.desc)
                            labelTopic.text = (dicData.topic)
                        }
                    }
                    nodeAvailable = true
                    viewDis.isHidden = false
                    viewDis.clipsToBounds = true
                    viewDis.layer.cornerRadius = 10
                } else {
                    let balls = self.fetchArray.map { $0.ballName }
                    if balls.contains(result.node.name!) {
                        let ballIndex = balls.firstIndex(of: result.node.name!)
                        if ballIndex != nil && ballIndex! < self.fetchArray.count {
                            let ballObject = self.fetchArray[ballIndex!]
                            textView.text = (ballObject.desc)
                            labelTopic.text = (ballObject.topic)

                        }
                    }
                }
            }
              if !nodeAvailable {
                ismanuallyAdded = true
                let anchor = ARAnchor(transform: hitTestResult.worldTransform)
                sceneView.session.add(anchor: anchor)
            }
        } else {
            viewDis.isHidden = true
            labelTopic.text = ""
            textView.text = ""
        }
    }

    func StoreLocationAPICall(email:String,location_id:Int,name:String, currentArrayIndex:NSInteger, ballName:String, description:String) {
//        Webservices.sharedInstance.StoreLocationWebService(email: email, location_id: location_id, name: name,ballName: ballName,description: description){(result, response,message),<#arg#>  in
//
//            if getStatusCode == 200 {
//
//                if let somecotegory = response {
//                    self.StoreLocationData = somecotegory
//                    var lastRoomObject = self.fetchArray[currentArrayIndex]
//                    lastRoomObject.ballid = self.StoreLocationData.data.id
//                    self.fetchArray[currentArrayIndex] = lastRoomObject
//                }
//            }
//            else if getStatusCode == 422 {
//                self.showAlert(withTitle: "EFridge", withMessage: "The selected email is invalid")
//            }
//            else if getStatusCode == 401 {
//                appDelegate.logoutMethod()
//            }
//            self.callStoreLocationAPIForNextIndex(index: currentArrayIndex+1)
//        }

    }

    func callStoreLocationAPIForNextIndex(index:NSInteger){
        if index < fetchArray.count {
            let storeObject = fetchArray[index]
            if storeObject.ballid == 0 {
                //email and location id, should used from previous screen selected object
//                StoreLocationAPICall(email: "user@demo.com", location_id: selectRoom.id, name: storeObject.topic, currentArrayIndex: index,ballName: storeObject.ballName, description: storeObject.desc)

            } else {
                callStoreLocationAPIForNextIndex(index: index+1)

            }

        }
    }
    func FatchLocationAPICall(location_id:Int){
//        Webservices.sharedInstance.FatchLocationWebService(location_id:location_id){(result, response,message) in
//
//            if getStatusCode == 200{
//                if let somecotegory = response{
//                    self.FatchLocationData = somecotegory
//                    self.addFetchedDataToArray()
//                }
//            }
//            else if getStatusCode == 422{
//                self.showAlert(withTitle: "EFridge", withMessage: "The selected email is invalid")
//            }
//            else if getStatusCode == 401{
//                appDelegate.logoutMethod()
//            }
//        }
    }

    func addFetchedDataToArray() {
        //assing FatchLocationData array to fetchArray
        for object in self.FatchLocationData {
            if let roomObject = object as? DataFatchLocationModel {
                let ballObject = FetchedBallData(topic: roomObject.name, desc: roomObject.descriptionAr, ballName: roomObject.ballName, ballid: roomObject.id)
                self.fetchArray.append(ballObject)
            }
        }
    }

    func generateSphereNode() -> SCNNode {

        let sphere = SCNSphere(radius: 0.5)
        let sphereNode = SCNNode()
        sphereNode.name = "sphereNODE \(selectRoom.id ?? 0) \(shhreNodeArry.count)"
        sphereNode.geometry = sphere
        sphereNode.geometry!.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Iicon")

        return sphereNode
    }

    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetTrackingConfiguration()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }

    @IBAction func resetBarButtonItemDidTouch(_ sender: UIButton) {
        resetTrackingConfiguration()
    }
    @IBAction func saveViewButton(_ sender: UIButton) {
        let balls = self.fetchArray.map { $0.ballName }
        //check if array already contains the selected object
        if !balls.contains(sphereNode.name!) {
            self.fetchArray.append(FetchedBallData(topic: labelTopic.text ?? "", desc: textView.text, ballName: sphereNode.name!))

        } else {
            //change contain of object if needed
        }

        self.view.endEditing(true)
        viewDis.isHidden = true
        labelTopic.text = ""
        textView.text = ""
        //        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

    @IBAction func saveBarButtonItemDidTouch(_ sender: UIButton) {
        sceneView.session.getCurrentWorldMap { (worldMap, error) in
            guard let worldMap = worldMap else {
                return self.setLabel(text: "Error getting current world map.")
            }
            do {
                try self.archive(worldMap: worldMap)
                DispatchQueue.main.async {

                    self.setLabel(text: "World map is saved.")
                    self.callStoreLocationAPIForNextIndex(index: 0)
                    //                    let storeObject = self.fetchArray[0]
                    //                    self.StoreLocationAPICall(email: "user@demo.com", location_id: self.selectRoom.id, name: storeObject.topic, currentArrayIndex: 0, ballName: storeObject.ballName, description: storeObject.desc)
                }
            } catch {
                fatalError("Error saving world map: \(error.localizedDescription)")
            }
        }
    }

    @IBAction func loadBarButtonItemDidTouch(_ sender: UIButton) {

        guard let worldMapData = retrieveWorldMapData(from: worldMapURL),
              let worldMap = unarchive(worldMapData: worldMapData) else { return }
        ismanuallyAdded = false
        resetTrackingConfiguration(with: worldMap)
    }

    @IBAction func backbtnAction(_ sender: UIButton) {
        //we need to pop to previous view controller
        self.navigationController?.popViewController(animated: true)
        //below will cause memory issue and app get crashed
        //        let vc = storyboard?.instantiateViewController(withIdentifier: "RoomTableController") as! RoomTableController
        //        self.navigationController?.pushViewController(vc, animated: true)

    }

    func resetTrackingConfiguration(with worldMap: ARWorldMap? = nil) {

        sphereNode = nil
        //  self.fetchArray = []
        viewDis.isHidden = true
        labelTopic.text = ""
        textView.text = ""
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal]

        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        if let worldMap = worldMap {
            configuration.initialWorldMap = worldMap
            setLabel(text: "Found saved world map.")
        } else {
            setLabel(text: "Move camera around to map your surrounding space.")
        }
        sceneView.debugOptions = [.showFeaturePoints]
        sceneView.session.run(configuration, options: options)

    }

    func setLabel(text: String) {
        label.text = text
    }

    func archive(worldMap: ARWorldMap) throws {
        let data = try NSKeyedArchiver.archivedData(withRootObject: worldMap, requiringSecureCoding: true)
        try data.write(to: self.worldMapURL, options: [.atomic])
    }

    func retrieveWorldMapData(from url: URL) -> Data? {
        do {
            return try Data(contentsOf: self.worldMapURL)
        } catch {
            self.setLabel(text: "Error retrieving world map data.")
            return nil
        }
    }

    func unarchive(worldMapData data: Data) -> ARWorldMap? {
        guard let unarchievedObject = ((try? NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data)) as ARWorldMap??),
              let worldMap = unarchievedObject else { return nil }
        return worldMap
    }

}

extension ViewController: ARSCNViewDelegate {

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard !(anchor is ARPlaneAnchor) else { return }
        //u need check spahre node is areaday created or not
        //if spahre node object == nil,then create obejct
        //sphereNode = generateSphereNode()

        print("mynide name-- \(String(describing: node.name))")
        sphereNode = generateSphereNode()
        node.addChildNode(sphereNode)
        shhreNodeArry.append(sphereNode)

//        // sphereNode.rendererDelegate = self;
//        DispatchQueue.main.async { [self] in
//            //createUIViewOnNode(current: node)
//            sphereNode = generateSphereNode()
////            if ismanuallyAdded {
////                sphereNode = generateSphereNode()
////            }else{
////                sphereNode = createUIViewOnNode(current: node,anchor: anchor)
////            }
//
//            node.addChildNode(sphereNode)
//
//            shhreNodeArry.append(sphereNode)
//
//        }
    }

    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {

        print("anchorname-- \(anchor.name ?? "")")

        let node = SCNNode()

//        let nodeGeometry = SCNBox(width: 0.2, height: 0.2, length: 0.2, chamferRadius: 0)
//        nodeGeometry.firstMaterial?.diffuse.contents = UIColor.cyan
//        node.geometry = nodeGeometry
//        node.position = SCNVector3(anchor.transform.columns.3.x, anchor.transform.columns.3.y, anchor.transform.columns.3.z)

        //node.addChildNode(currentNode!)
        //
//        if let imageAnchor = anchor as? ARImageAnchor {
//            let size = imageAnchor.referenceImage.physicalSize
//            updateSize = size
//            node = self.upDateNode(frameSize: size)
//        }

        return node
    }

}

//MARK: - Create Node Viewcontroller

extension ViewController {
    func createUIViewOnNode(current:SCNNode,anchor:ARAnchor) -> SCNNode {

        print("current position-- \(current.position)")

        let sphere = SCNSphere(radius: 0.05)
        var sphereNode = SCNNode()
        sphereNode = current
        //sphereNode.name = "sphereNODE \(selectRoom.id ?? 0) \(shhreNodeArry.count)"
        sphereNode.geometry = sphere
        sphereNode.geometry!.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Iicon")

        //1. Create An Empty Node
        let holderNode = SCNNode()
        // holderNode = current
        //2. Set It's Geometry As An SCNPlane

        holderNode.geometry = SCNPlane(width: 0.2, height: 0.1)

        //2. Create A New Material
        let material = SCNMaterial()
        //material.bac
        let textLbl = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 30))
        textLbl.font = UIFont.systemFont(ofSize: 13)
        textLbl.backgroundColor = .white
        textLbl.textColor = .black
        textLbl.text = "My Title" //finalTitle
        textLbl.textAlignment = .center
        textLbl.numberOfLines = 0

        //5. Set The Materials Contents
        material.diffuse.contents = textLbl

        //6. Set The 1st Material Of The Plane
        holderNode.geometry?.firstMaterial = material

        material.isDoubleSided = false

        //holderNode.position = SCNVector3(0, 0, -1.5)
        let position = current.position
        holderNode.position = SCNVector3(position.x/3, -position.y/9, position.z/4)
        sphereNode.addChildNode(holderNode)
        return sphereNode
    }
}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}

extension UIColor {
    open class var transparentWhite: UIColor {
        return UIColor.white.withAlphaComponent(0.70)
    }
}

