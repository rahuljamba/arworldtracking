//
//  ValidateOTPVC.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 06/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import UIKit

var strToken:String = ""
class ValidateOTPVC: UIViewController, UITextFieldDelegate {

    enum DisplayType: Int {
        case circular
        case roundedCorner
        case square
        case diamond
        case underlinedBottom
    }
    
    
    var ValidateData = ValidateModel()
    var emailStr = String()
    
    @IBOutlet var otpTextFieldView: UITextField!{
        didSet { otpTextFieldView?.addDoneCancelToolbar() }
    }
    @IBOutlet weak var validateOtpBtn: UIButton!
    @IBOutlet weak var emaillable: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emaillable.text!  = emailStr
        validateOtpBtn.layer.cornerRadius = 10.0

       // setupOtpView()
        // Do any additional setup after loading the view.
    }

    func ValidateAPICall(email: String,passcode: String){
        CommonClass.showLoader()
        Webservices.sharedInstance.ValidateWebService(email:email,passcode:passcode){(result, response,message) in
            CommonClass.hideLoader()
            
            if getStatusCode == 200{
                
                    if let somecotegory = response{
                    UserDefaults.standard.setValue(true, forKey: "loginStatusKey")
                    self.ValidateData = somecotegory
                    strToken = self.ValidateData.auth_token
                    UserDefaults.standard.setValue(self.ValidateData.auth_token, forKey: "authToken")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "RoomTableController") as! RoomTableController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if getStatusCode == 401{
                appDelegate.logoutMethod()
            }
            else if getStatusCode == 422{
                self.showAlert(withTitle: "EFridge", withMessage: "Invalid passcode entered!")
        }
    }
}
    
    @IBAction func validateBtnAction(_ sender: UIButton) {
        if otpTextFieldView.text != ""{
            ValidateAPICall(email: emailStr, passcode:otpTextFieldView.text!)
        }
    }

}
extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))

        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()

        self.inputAccessoryView = toolbar
    }

    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    @objc func cancelButtonTapped() { self.resignFirstResponder() }
}
