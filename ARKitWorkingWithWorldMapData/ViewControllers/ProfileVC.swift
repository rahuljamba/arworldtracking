//
//  ProfileVC.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 07/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, UITextFieldDelegate {

  
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var mobileNumberTextfield: UITextField!
    @IBOutlet weak var addressTextfield: UITextField!
    @IBOutlet weak var countryTextfield: UITextField!
    
    @IBOutlet weak var updateBtn: UIButton!
    
    @IBOutlet weak var stateTextfield: UITextField!
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var pincodeTextfield: UITextField!
    @IBOutlet weak var genderTextfield: UITextField!
    
    @IBOutlet weak var profileImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        // Do any additional setup after loading the view.
    }
    func setUI() {
        upperView.layer.masksToBounds = true
        upperView.layer.masksToBounds = false
        upperView.layer.shadowOffset = CGSize(width: 0, height: 0)
        upperView.layer.shadowColor = UIColor.black.cgColor
        upperView.layer.shadowOpacity = 0.23
        upperView.layer.shadowRadius = 4
        
        profileImg.layer.cornerRadius = 10.0
        updateBtn.layer.cornerRadius = 10.0
        
        pincodeTextfield.delegate = self
        pincodeTextfield.returnKeyType = .done
        
        genderTextfield.delegate = self
        genderTextfield.returnKeyType = .done
        
        cityTextfield.delegate = self
        cityTextfield.returnKeyType = .done
        
        stateTextfield.delegate = self
        stateTextfield.returnKeyType = .done
        
        countryTextfield.delegate = self
        countryTextfield.returnKeyType = .done
        
        addressTextfield.delegate = self
        addressTextfield.returnKeyType = .done
        
        mobileNumberTextfield.delegate = self
        mobileNumberTextfield.returnKeyType = .done
        
        nameTextfield.delegate = self
        nameTextfield.returnKeyType = .done
        
        nameTextfield.layer.cornerRadius = 10.0
        
        nameTextfield.clipsToBounds = true
        nameTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        nameTextfield.layer.borderWidth = 1.0
        
        mobileNumberTextfield.layer.cornerRadius = 10.0
        
        mobileNumberTextfield.clipsToBounds = true
        mobileNumberTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        mobileNumberTextfield.layer.borderWidth = 1.0
        
        addressTextfield.layer.cornerRadius = 10.0
        
        addressTextfield.clipsToBounds = true
        addressTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        addressTextfield.layer.borderWidth = 1.0
        
        countryTextfield.layer.cornerRadius = 10.0
        
        countryTextfield.clipsToBounds = true
        countryTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        countryTextfield.layer.borderWidth = 1.0

        
        stateTextfield.layer.cornerRadius = 10.0
        
        stateTextfield.clipsToBounds = true
        stateTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        stateTextfield.layer.borderWidth = 1.0
        
        cityTextfield.layer.cornerRadius = 10.0
        
        cityTextfield.clipsToBounds = true
        cityTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        cityTextfield.layer.borderWidth = 1.0
        
        pincodeTextfield.layer.cornerRadius = 10.0
        
        pincodeTextfield.clipsToBounds = true
        pincodeTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        pincodeTextfield.layer.borderWidth = 1.0
        
        genderTextfield.layer.cornerRadius = 10.0
        
        genderTextfield.clipsToBounds = true
        genderTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        genderTextfield.layer.borderWidth = 1.0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        genderTextfield.resignFirstResponder()
        pincodeTextfield.resignFirstResponder()
        cityTextfield.resignFirstResponder()
        stateTextfield.resignFirstResponder()
        countryTextfield.resignFirstResponder()
        addressTextfield.resignFirstResponder()
        mobileNumberTextfield.resignFirstResponder()
        nameTextfield.resignFirstResponder()
        return true
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditVC") as! EditVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
