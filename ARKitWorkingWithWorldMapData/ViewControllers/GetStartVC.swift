//
//  GetStartVC.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 06/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import UIKit

class GetStartVC: UIViewController {
    
    @IBOutlet weak var getStartBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        getStartBtn.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
    }
    
    @IBAction func getBtnAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
