//
//  TrackingBallVC.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by mac on 29/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

struct FetchedBall {
    var topic: String
    var desc: String
    var ballName: String
    var ballid : Int = 0
}

import UIKit
import ARKit
import SceneKit
import SVProgressHUD

class TrackingBallVC: UIViewController, ARSCNViewDelegate, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var sceneView: ARSCNView!
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelTopic: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var viewDis: UIView!
    @IBOutlet weak var saveViewBtn: UIButton!
    
    var ballListArray = [DataFatchLocationModel]()
    var selectRoom:DataListModel!
    var uniqueAnchorID = ""
    var isLoadBall = false
    var isSaved = false
    var newBallsArray = [FetchedBall]()
    let myBallGroup = DispatchGroup()
    
    lazy var worldMapURL: URL = {
        let worldMapString = "worldMapURL\(selectRoom.id)"
        do {
            return try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent(worldMapString)
        } catch {
            fatalError("Error getting world map URL from document directory.")
        }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.scene = SCNScene()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        addTapGestureToSceneView()
        setUI()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SVProgressHUD.show()
        getSavedBallList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        resetTrackingConfiguration()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
}

extension TrackingBallVC {
    
    @IBAction func backbtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetBarButtonItemDidTouch(_ sender: UIButton) {
        resetTrackingConfiguration()
    }
    
    @IBAction func saveBallWithTitleButtonAction(_ sender:UIButton) {
        if labelTopic.text?.isEmpty ?? false || textView.text.isEmpty {
            let alert = UIAlertController(title: "Oops!", message: "Please full fill ball information.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        let ballValue = FetchedBall(topic: labelTopic.text ?? "", desc: textView.text ?? "", ballName: uniqueAnchorID)
        newBallsArray.append(ballValue)
        viewDis.isHidden = true
    }
    
    @IBAction func saveFinalBallListButtonAction(_ sender:UIButton) {
        saveWorldMapView()
    }
    
    @IBAction func loadSavedBallWithTitleButtonAction(_ sender:UIButton) {
        isLoadBall = true
        isSaved = true
        guard let worldMapData = retrieveWorldMapData(from: worldMapURL),
            let worldMap = unarchive(worldMapData: worldMapData) else { return }
        resetTrackingConfiguration(with: worldMap)
    }
}

extension TrackingBallVC {
    
    func saveWorldMapView() {
        sceneView.session.getCurrentWorldMap { (worldMap, error) in
            guard let worldMap = worldMap else {
                return self.setLabel(text: "Error getting current world map.")
            }
            
            do {
                try self.archive(worldMap: worldMap)
                SVProgressHUD.show()
                self.callMultiplePostAPis()
            } catch {
                fatalError("Error saving world map: \(error.localizedDescription)")
            }
        }
    }
    
}

extension TrackingBallVC {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard !(anchor is ARPlaneAnchor) else { return }
        
        if isLoadBall {
            DispatchQueue.main.async {
                let reciveAnchorID = anchor.identifier.uuidString
                var ballTitle = ""
                if let findObject = self.searchElephantIndex(name: reciveAnchorID, elephArray: self.ballListArray) {
                    ballTitle = findObject.name
                }
                let reciveNode = self.genrateNodeWithLabel(current: node, titleText: ballTitle, anchor: anchor)
                node.name = anchor.identifier.uuidString
                node.addChildNode(reciveNode)
            }
        }else{
            let sphereNode = generateSphereNode()
            DispatchQueue.main.async {
                node.addChildNode(sphereNode)
            }
        }
    }
    
    func addTapGestureToSceneView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(rec:)))
        //Add recognizer to sceneview
        sceneView.addGestureRecognizer(tap)
    }
    
    //Method called when tap
    @objc func handleTap(rec: UITapGestureRecognizer) {
        if viewDis.isHidden == false {
            let alert = UIAlertController(title: "Oops!", message: "Please full fill ball information.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
           return
        }
        
        let location: CGPoint = rec.location(in: sceneView)
        if rec.state == .ended {
            let hits = self.sceneView.hitTest(location, options: nil)
            if !hits.isEmpty{
                guard let hitTestResult = sceneView.hitTest(location, types: [.featurePoint, .estimatedHorizontalPlane] ).first
                else { return }
             
                let tappedNode = hits.first?.node
                print("hitthe name-- \(tappedNode?.name ?? "")")
                let currentAnchorID = tappedNode?.name ?? ""
              
                if isSaved {
                    if let findObject = self.searchElephantIndex(name: currentAnchorID, elephArray: self.ballListArray) {
                        labelTopic.text = findObject.name
                        textView.text = findObject.descriptionAr
                    }
                }else{
                    labelTopic.text = ""
                    textView.text = ""
                }
                viewDis.isHidden = false
                
            }else{
                addanchorInWorldmap(location: location)
            }
        }else{
            addanchorInWorldmap(location: location)
        }
    }
    
    func addanchorInWorldmap(location:CGPoint) {
        guard let hitTestResult = sceneView.hitTest(location, types: [.featurePoint, .estimatedHorizontalPlane] ).first
        else { return }
        let anchor = ARAnchor(transform: hitTestResult.worldTransform)
        let anchorId = anchor.identifier.uuidString
        uniqueAnchorID = anchorId
        isLoadBall = false
        sceneView.session.add(anchor: anchor)
    }
    
    func generateSphereNode() -> SCNNode {
        let sphere = SCNSphere(radius: 0.05)
        let sphereNode = SCNNode()
        sphereNode.name = uniqueAnchorID
        sphereNode.position.y += Float(sphere.radius)
        sphereNode.geometry = sphere
        sphereNode.geometry!.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Iicon")
        return sphereNode
    }
    
    func genrateNodeWithLabel(current:SCNNode,titleText:String,anchor:ARAnchor) -> SCNNode {
        print("current position-- \(current.position)")
        let sphere = SCNSphere(radius: 0.05)
        var sphereNode = SCNNode()
        
        sphereNode = current
        sphereNode.geometry = sphere
        sphereNode.geometry!.firstMaterial?.diffuse.contents = #imageLiteral(resourceName: "Iicon")

        //1. Create An Empty Node
        let holderNode = SCNNode()
        holderNode.geometry = SCNPlane(width: 0.09, height: 0.05)

        //2. Create A New Material
        let material = SCNMaterial()
        let textLbl = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 30))
        textLbl.font = UIFont.systemFont(ofSize: 12)
        textLbl.backgroundColor = .white
        textLbl.textColor = .black
        textLbl.text = titleText
        textLbl.textAlignment = .center
        textLbl.numberOfLines = 0
        textLbl.adjustsFontSizeToFitWidth = true
        textLbl.minimumScaleFactor = 0.7
        textLbl.lineBreakMode = .byClipping

        //5. Set The Materials Contents
        material.diffuse.contents = textLbl
        material.isDoubleSided = true

        holderNode.geometry?.firstMaterial = material

        let position = current.position

        holderNode.position = SCNVector3(0.0, position.y+0.23, position.z)

        sphereNode.addChildNode(holderNode)

        return sphereNode
    }
    
    
    func resetTrackingConfiguration(with worldMap: ARWorldMap? = nil) {
        viewDis.isHidden = true
        //isLoadBall = false
        uniqueAnchorID = ""
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal]//,.vertical]
        if let worldMap = worldMap {
            configuration.initialWorldMap = worldMap
            setLabel(text: "Found saved world map.")
        } else {
            configuration.initialWorldMap = .none
            setLabel(text: "Move camera around to map your surrounding space.")
        }
        
        // Code Comment Begin the time.
        //sceneView.session.run(configuration)
        //let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        //sceneView.debugOptions = [.showFeaturePoints]
        
        sceneView.session.run(configuration, options: .resetTracking)
        
    }
}

extension TrackingBallVC {
    func archive(worldMap: ARWorldMap) throws {
        let data = try NSKeyedArchiver.archivedData(withRootObject: worldMap, requiringSecureCoding: true)
        try data.write(to: self.worldMapURL, options: [.atomic])
    }
    func retrieveWorldMapData(from url: URL) -> Data? {
        do {
            return try Data(contentsOf: self.worldMapURL)
        } catch {
            self.setLabel(text: "Error retrieving world map data.")
            return nil
        }
    }
    func unarchive(worldMapData data: Data) -> ARWorldMap? {
        guard let unarchievedObject = (try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)) as? ARWorldMap else {
            return nil
        }
        return unarchievedObject
    }
    func setLabel(text: String) {
        label.text = text
    }
    func setUI() {
        labelTopic.delegate = self
        labelTopic.returnKeyType = .done
        
        textView.delegate = self
        textView.returnKeyType = .done
        
        labelTopic.layer.cornerRadius = 10.0
        
        labelTopic.clipsToBounds = true
        labelTopic.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        labelTopic.layer.borderWidth = 1.0
        
        textView.layer.cornerRadius = 10.0
        
        textView.clipsToBounds = true
        textView.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        textView.layer.borderWidth = 1.0
        
        saveViewBtn.layer.cornerRadius = 10.0
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        labelTopic.resignFirstResponder()
        textView.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //heightConstraint?.constant = UIScreen.main.bounds.height - (UIScreen.main.bounds.height/2)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)  {
        //heightConstraint?.constant = 0
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        //heightConstraint?.constant = UIScreen.main.bounds.height - (UIScreen.main.bounds.height/2)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        //heightConstraint?.constant = 0
    }
    
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
}

extension TrackingBallVC {
    
    
    func getSavedBallList() {
        
        Webservices.sharedInstance.FatchLocationWebService(location_id:self.selectRoom.id){(result, response,message,statusCode) in
            SVProgressHUD.dismiss()
            if statusCode == 200 {
                if let ballList = response {
                    self.ballListArray = ballList
                }
            }
            else if statusCode == 422 {
                self.showAlert(withTitle: "EFridge", withMessage: "The selected email is invalid")
            }
            else if statusCode == 401 {
                appDelegate.logoutMethod()
            }
        }
        
    }
    
    
    func callMultiplePostAPis() {
        
        self.newBallsArray.forEach { (ball) in
            self.myBallGroup.enter()
            self.storeLocationAPICall(ballInfo: ball)
        }
        
        self.myBallGroup.notify(queue: .main) {
            print("Finished all requests.")
            SVProgressHUD.dismiss()
            self.viewDis.isHidden = true
            DispatchQueue.main.async {
                self.setLabel(text: "World map is saved.")
            }
        }
    }
    
    
    func storeLocationAPICall(ballInfo:FetchedBall) {
        Webservices.sharedInstance.StoreLocationWebService(email: "user@demo.com", location_id: self.selectRoom.id, name: ballInfo.topic, ballName: ballInfo.ballName, description: ballInfo.desc) { (result, response, message,statusCode) in
            
            
            
            DispatchQueue.main.async {
                self.myBallGroup.leave()
            }
            if statusCode == 200 {
                
            }
            if statusCode == 422 {
                self.showAlert(withTitle: "EFridge", withMessage: "The selected email is invalid")
            }
            if statusCode == 401 {
                appDelegate.logoutMethod()
            }
        }
    }
    
}

extension TrackingBallVC {
    func searchElephantIndex(name: String, elephArray: [DataFatchLocationModel]) -> DataFatchLocationModel? {
        if elephArray.count == 0 {
            return nil
        }
        let findIndex = elephArray.firstIndex { $0.ballName == name } ?? 0
        let findObject = elephArray[findIndex]
        return findObject
    }
}
