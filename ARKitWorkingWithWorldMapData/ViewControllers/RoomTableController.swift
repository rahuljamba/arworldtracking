//
//  RoomTableController.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 16/04/21.
//  Copyright © 2021 Vaibhav Awasthi. All rights reserved.
//

import UIKit
import SDWebImage

class RoomTableController: UIViewController {
    
    var tableRoomData = TableRoomParser()
    @IBOutlet var TableView: UITableView!
   
    var element = ["Bedroom","Kitchen","Training Room","Meeting Room"]
    var imageArray =  [#imageLiteral(resourceName: "Training Room"),#imageLiteral(resourceName: "Kitchen"),#imageLiteral(resourceName: "Bedroom"),#imageLiteral(resourceName: "Meeting Room")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        TableView.delegate = self
        TableView.dataSource = self
        tableRoomPICall()
        // Do any additional setup after loading the view.
    }
    
    
   func tableRoomPICall(){
   
    Webservices.sharedInstance.TableRoomListWebService(){(result, response,message) in
    
            if getStatusCode == 200{
                if let somecotegory = response{
                self.tableRoomData = somecotegory
                    self.TableView.reloadData()
                }
            }
            else if getStatusCode == 422{
                self.showAlert(withTitle: "EFridge", withMessage: "The selected email is invalid")
            }
            else if getStatusCode == 401{
                appDelegate.logoutMethod()
            }
     }
  }
    
    @IBAction func leftBtnAction(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func editBtnAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditVC") as! EditVC
        self.navigationController?.pushViewController(vc, animated: true)
    }


}


extension RoomTableController: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableRoomData.data.count//element.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoomTableCellTableViewCell") as! RoomTableCellTableViewCell
        
        cell.SettingView.layer.cornerRadius = 8
        cell.SettingView.layer.masksToBounds = true
        cell.SettingView.layer.masksToBounds = false
        cell.SettingView.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.SettingView.layer.shadowColor = UIColor.black.cgColor
        cell.SettingView.layer.shadowOpacity = 0.23
        cell.SettingView.layer.shadowRadius = 4
        
        let imgUrl = tableRoomData.data[indexPath.row].image
        cell.ImgView.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "Meeting Room"))
        cell.labelLocationAddress.text = tableRoomData.data[indexPath.row].floor
        cell.ImgLbl.text = tableRoomData.data[indexPath.row].name
        
//        cell.ImgLbl.text = element[indexPath.row]
//        cell.ImgView.image = imageArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "TrackingBallVC") as! TrackingBallVC
        vc.selectRoom = tableRoomData.data[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    
    }

}
