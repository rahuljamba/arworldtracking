//
//  EditVC.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 11/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import UIKit
import SDWebImage
class EditVC: UIViewController {

    var logoutData = LogoutModel()
    var ProfileData = ProfileModel()

   
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var mainViewEdit: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var numberlabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var genderlabel: UILabel!
    @IBOutlet weak var profileEditImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        profileAPICall()
        setUp()
    }
    func setUp() {
        
        profileEditImage.layer.cornerRadius = 39
        profileEditImage.layer.borderWidth = 1.0
        profileEditImage.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        mainViewEdit.layer.cornerRadius = 8
        mainViewEdit.layer.masksToBounds = true

        mainViewEdit.layer.masksToBounds = false
        mainViewEdit.layer.shadowOffset = CGSize(width: 0, height: 0)
        mainViewEdit.layer.shadowColor = UIColor.black.cgColor
        mainViewEdit.layer.shadowOpacity = 0.23
        mainViewEdit.layer.shadowRadius = 4
        
        upperView.layer.masksToBounds = true

        upperView.layer.masksToBounds = false
        upperView.layer.shadowOffset = CGSize(width: 0, height: 0)
        upperView.layer.shadowColor = UIColor.black.cgColor
        upperView.layer.shadowOpacity = 0.23
        upperView.layer.shadowRadius = 4
    }
    
    
    func profileAPICall(){
        CommonClass.showLoader()
        Webservices.sharedInstance.ProfileWebService(){(result, response,message) in
            CommonClass.hideLoader()
            if getStatusCode == 200{
                if let somecotegory = response{
                    
                    self.ProfileData = somecotegory
                    let imgUrl = self.ProfileData.data.profile_pic
                    self.profileEditImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "Profile Img"))
                    
                    self.nameLabel.text = self.ProfileData.data.name
                    self.emailLabel.text = self.ProfileData.data.email
                    self.numberlabel.text = self.ProfileData.data.phone
                    self.companyNameLabel.text = self.ProfileData.data.company
                    self.addressLabel.text = self.ProfileData.data.address
                    self.genderlabel.text = self.ProfileData.data.gender
                    print(somecotegory)
                }
                else if getStatusCode == 401{
                    appDelegate.logoutMethod()
                }
              }else if getStatusCode == 401{
                self.showAlert(withTitle: "Efridge", withMessage: "Something went to wrong")
            }
        }
    }
    
    func LogoutAPICall(){
        CommonClass.showLoader()
        Webservices.sharedInstance.logoutWebService(){(result, response,message) in
            CommonClass.hideLoader()
            UserDefaults.standard.setValue(false, forKey: "loginStatusKey")
            appDelegate.logoutMethod()
            if getStatusCode == 200{
                if let somecotegory = response{
                self.logoutData = somecotegory
                }
            }
     }
  }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoutBtnAction(_ sender: UIButton) {
        LogoutAPICall()

    }
    @IBAction func editBtnAction(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
