//
//  RegisterVC.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 07/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var mobileNumberTextfield: UITextField!
    @IBOutlet weak var addressTextfield: UITextField!
    @IBOutlet weak var companyTextfield: UITextField!
    @IBOutlet weak var resgisterBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        // Do any additional setup after loading the view.
    }
    func setUI() {
        
        resgisterBtn.layer.cornerRadius = 10.0
        
        nameTextfield.layer.cornerRadius = 10.0
        
        nameTextfield.clipsToBounds = true
        nameTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        nameTextfield.layer.borderWidth = 1.0
        
        mobileNumberTextfield.layer.cornerRadius = 10.0
        
        mobileNumberTextfield.clipsToBounds = true
        mobileNumberTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        mobileNumberTextfield.layer.borderWidth = 1.0
        
        addressTextfield.layer.cornerRadius = 10.0
        
        addressTextfield.clipsToBounds = true
        addressTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        addressTextfield.layer.borderWidth = 1.0
        
        companyTextfield.layer.cornerRadius = 10.0
        
        companyTextfield.clipsToBounds = true
        companyTextfield.layer.borderColor = #colorLiteral(red: 0.2742073834, green: 0.4376631975, blue: 0.6221209168, alpha: 1)
        companyTextfield.layer.borderWidth = 1.0
    }
    
    @IBAction func registerBtnAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
      //  worldMapGlobalKey = "1"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
