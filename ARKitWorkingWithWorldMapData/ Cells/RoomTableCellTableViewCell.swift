//
//  RoomTableCellTableViewCell.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 16/04/21.
//  Copyright © 2021 Vaibhav Awasthi. All rights reserved.
//

import UIKit

class RoomTableCellTableViewCell: UITableViewCell {

    
    @IBOutlet var ImgView: UIImageView!
    @IBOutlet var ImgLbl: UILabel!
    @IBOutlet weak var SettingView : UIView!
    @IBOutlet weak var labelLocationAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ImgView.layer.cornerRadius = 10
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
