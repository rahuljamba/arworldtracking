//
//  parser.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 10/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import Foundation
import SwiftyJSON


class LoginParser : NSObject{
    let Kdata = "data"
    let status = "status"
    
    var result : Int = 0
    var responseMessage = ""
    var data = LoginModel()
    
    
    
    override init() {
        super.init()
    }
    init(json: JSON) {
       
        if let status = json[status].string as String?{
            self.responseMessage = status
        }
        
        if let common = json.dictionaryObject as [String: AnyObject]?{
            self.data = LoginModel(dictionary: common)
        }
        super.init()
    }
}


class ValidateParser : NSObject{
    let Kdata = "data"
    let status = "status"
    
    var result : Int = 0
    var responseMessage = ""
    var data = ValidateModel()
    
    
    
    override init() {
        super.init()
    }
    init(json: JSON) {
       
        if let status = json[status].string as String?{
            self.responseMessage = status
        }
        
        if let common = json.dictionaryObject as [String: AnyObject]?{
            self.data = ValidateModel(dictionary: common)
        }
        super.init()
    }
}


class ProfileParser : NSObject{
    let Kdata = "data"
    let status = "status"
    
    var result : Int = 0
    var responseMessage = ""
    var data = ProfileModel()
    
    
    
    override init() {
        super.init()
    }
    init(json: JSON) {
       
        if let status = json[status].string as String?{
            self.responseMessage = status
        }
        
        if let common = json.dictionaryObject as [String: AnyObject]?{
            self.data = ProfileModel(dictionary: common)
        }
        super.init()
    }
}


class logoutParser : NSObject{
    let Kdata = "data"
    let status = "status"
    
    var result : Int = 0
    var responseMessage = ""
    var data = LogoutModel()
    
    
    
    override init() {
        super.init()
    }
    init(json: JSON) {
       
        if let status = json[status].string as String?{
            self.responseMessage = status
        }
        
        if let common = json.dictionaryObject as [String: AnyObject]?{
            self.data = LogoutModel(dictionary: common)
        }
        super.init()
    }
}


class StoreLocationParser : NSObject{
    let Kdata = "data"
    let status = "status"
    
    var result : Int = 0
    var responseMessage = ""
    var data = StoreObjectLocationModel()
    
    
    
    override init() {
        super.init()
    }
    init(json: JSON) {
       
        if let status = json[status].string as String?{
            self.responseMessage = status
        }
        
        if let common = json.dictionaryObject as [String: AnyObject]?{
            self.data = StoreObjectLocationModel(dictionary: common)
        }
        super.init()
    }
}

class FatchLocationParser : NSObject{
    let kResponseMessage = "msg"
    let kResult = "result"
    let Kdata = "data"
    
    var result : Int = 0
    var responseMessage = ""
    var data = [DataFatchLocationModel]()
    
    override init() {
        super.init()
    }
    init(json: JSON) {

        let dictData = json.dictionary
        let arrayLocation = dictData?["data"]?.arrayValue
        
        for i in 0 ..< (arrayLocation?.count ?? 0) {
            let dic = arrayLocation?[i].dictionary
            let faq = DataFatchLocationModel(dictionary: dic!)
            self.data.append(faq)
        }
        super.init()
    }
}

class TableRoomParser : NSObject{
    let kResponseMessage = "msg"
    let kResult = "result"
    let Kdata = "data"
    
    var result : Int = 0
    var responseMessage = ""
    var data = [DataListModel]()
    
    override init() {
        super.init()
    }
    init(json: JSON) {
        super.init()

        let dictData = json.dictionary
        let arrayLocation = dictData?["data"]?.arrayValue
        
        for i in 0 ..< (arrayLocation?.count ?? 0) {
            let dic = arrayLocation?[i].dictionary
            let Faq = DataListModel(dictionary: dic!)
            self.data.append(Faq)
        }
        
    }
}
