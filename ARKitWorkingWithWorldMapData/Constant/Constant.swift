//
//  Constant.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 14/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//

import Foundation

var baseUrl = "http://admin.vrsites.co/api/v1/"
var imgBaseUrl = "http://admin.vrsites.co"
var login = "login"
var logout = "logout"
var profile = "profile"
var varify = "passcode/verify"
var storeLocation = "locations/objects/store"

