//
//  webservices.swift
//  ARKitWorkingWithWorldMapData
//
//  Created by Amit chaudhary on 10/05/21.
//  Copyright © 2021 Jayven Nhan. All rights reserved.
//


import Foundation
import Alamofire
import SwiftyJSON

var getStatusCode :Int = 0
class Webservices : NSObject {
    
    static let sharedInstance = Webservices()
    
    func checkAuthToken() -> Bool {
        
        if let authToken =  UserDefaults.standard.value(forKey: "authToken") {
            strToken = authToken as! String
        }else{
            return false
        }
        return true
    }
        
   func LoginWebService(email:String,completionBlock:@escaping (_ success: Int, _ data : LoginModel?, _ message: String) -> Void) {

        var params = Dictionary<String, Any>()
    
        params.updateValue(email, forKey: "email")
        
        print("==========\(params)")
        let header = ["Accept": "application/json"]
        let url = baseUrl + login
    
       
    Alamofire.request(url,method: .post,parameters : params, headers: header).responseJSON { response in
        
        getStatusCode = 0
        let statusCode = (response.response?.statusCode) ?? 0
        print(statusCode)
        getStatusCode = statusCode
            
            switch response.result {
            case.success:
                if let value = response.result.value {
                    let json = JSON(value)
                  
                    print("=========login json is:\n\(json)")
                    let parser = LoginParser(json: json)
                  
                    completionBlock(parser.result,parser.data,parser.responseMessage)
                }else{
                    completionBlock(0,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    
                }
            case .failure(let error):
                completionBlock(0,nil,error.localizedDescription)
            }
        }
    }
    
    
    func ValidateWebService(email:String,passcode:String,completionBlock:@escaping (_ success: Int, _ data : ValidateModel?, _ message: String) -> Void) {

         var params = Dictionary<String, Any>()
    
        params.updateValue(email, forKey: "email")
        params.updateValue(passcode, forKey: "passcode")
        
        print("==========\(params)")
        let header = ["Accept": "application/json"]
        let url = baseUrl + varify
        
       
    Alamofire.request(url,method: .post,parameters : params, headers: header).responseJSON { response in
        
        getStatusCode = 0
        let statusCode = (response.response?.statusCode) ?? 0
        print(statusCode)
        getStatusCode = statusCode
            
            
            switch response.result {
            case.success:
                if let value = response.result.value {
                    let json = JSON(value)
                  
                    print("=========Validate json is:\n\(json)")
                    let parser = ValidateParser(json: json)
                  
                    completionBlock(parser.result,parser.data,parser.responseMessage)
                }else{
                    completionBlock(0,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    
                }
            case .failure(let error):
                completionBlock(0,nil,error.localizedDescription)
            }
        }
    }
    
    
    func ProfileWebService(completionBlock:@escaping (_ success: Int, _ data : ProfileModel?, _ message: String) -> Void) {
        
        if checkAuthToken() == false {
            return
        }
        
        let auth = "Bearer " + strToken
        print(auth)
        let header = ["Accept": "application/json","Authorization": auth]
        let url = baseUrl + profile
       
        Alamofire.request(url,method: .get, headers: header).responseJSON { response in
        
        getStatusCode = 0
        let statusCode = (response.response?.statusCode) ?? 0
        print(statusCode)
        getStatusCode = statusCode
            
            
            switch response.result {
            case.success:
                if let value = response.result.value {
                    let json = JSON(value)
                  
                    print("=========Profile json is:\n\(json)")
                    let parser = ProfileParser(json: json)
                  
                    completionBlock(parser.result,parser.data,parser.responseMessage)
                }else{
                    completionBlock(0,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    
                }
            case .failure(let error):
                completionBlock(0,nil,error.localizedDescription)
            }
        }
    }

    
    func logoutWebService(completionBlock:@escaping (_ success: Int, _ data : LogoutModel?, _ message: String) -> Void) {
        if checkAuthToken() == false {
            return
        }
        
        let auth = "Bearer " + strToken
        print(auth)
        let header = ["Accept": "application/json","Authorization": auth]
        let url = baseUrl + logout
       
        Alamofire.request(url,method: .get, headers: header).responseJSON { response in
        
        getStatusCode = 0
        let statusCode = (response.response?.statusCode) ?? 0
        print(statusCode)
        getStatusCode = statusCode
            
            switch response.result {
            case.success:
                if let value = response.result.value {
                    let json = JSON(value)
                  
                    print("=========logout json is:\n\(json)")
                    let parser = logoutParser(json: json)
                  
                    completionBlock(parser.result,parser.data,parser.responseMessage)
                }else{
                    completionBlock(0,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    
                }
            case .failure(let error):
                completionBlock(0,nil,error.localizedDescription)
            }
        }
    }
    
    
    func StoreLocationWebService(email:String,location_id:Int,name:String,ballName:String, description:String, completionBlock:@escaping (_ success: Int, _ data : StoreObjectLocationModel?, _ message: String,_ statusCode:Int) -> Void) {
        if checkAuthToken() == false {
            return
        }
        
        var params = Dictionary<String, Any>()
        params.updateValue(email, forKey: "email")
        params.updateValue(location_id, forKey: "location_id")
        params.updateValue(name, forKey: "name")
        params.updateValue(ballName, forKey: "ballName")
        params.updateValue(description, forKey: "description")

        let auth = "Bearer " + strToken
        print(auth)
        let header = ["Accept": "application/json","Authorization": auth]
        let url = baseUrl + storeLocation
       
        Alamofire.request(url,method: .post,parameters: params,headers: header).responseJSON { response in
        
        getStatusCode = 0
        let statusCode = (response.response?.statusCode) ?? 0
        print(statusCode)
        getStatusCode = statusCode
            
            switch response.result {
            case.success:
                if let value = response.result.value {
                    let json = JSON(value)
                  
                    print("=========Store Loaction json is:\n\(json)")
                    let parser = StoreLocationParser(json: json)
                  
                    completionBlock(parser.result,parser.data,parser.responseMessage, statusCode)
                }else{
                    completionBlock(0,nil,response.result.error?.localizedDescription ?? "Some thing went wrong", statusCode)
                    
                }
            case .failure(let error):
                completionBlock(0,nil,error.localizedDescription, statusCode)
            }
        }
    }
    
    
    func FatchLocationWebService(location_id:Int,completionBlock:@escaping (_ success: Int, _ data : [DataFatchLocationModel]?, _ message: String,_ statusCode:Int) -> Void) {
        if checkAuthToken() == false {
            return
        }
        
        var params = Dictionary<String, Any>()
        params.updateValue(location_id, forKey: "location_id")
        
        let auth = "Bearer " + strToken
        print(auth)
        let header = ["Accept": "application/json","Authorization": auth]
        let url = "http://admin.vrsites.co/api/v1/locations/objects/index?location_id=\(location_id)"
       
        Alamofire.request(url,method: .get,parameters: params,headers: header).responseJSON { response in
            
            getStatusCode = 0
            let statusCode = (response.response?.statusCode) ?? 0
            print(statusCode)
            getStatusCode = statusCode
            
            switch response.result {
            case.success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    print("=========fatch Loaction json is:\n\(json)")
                    let parser = FatchLocationParser(json: json)
                    completionBlock(parser.result,parser.data,parser.responseMessage, statusCode)
                }else{
                    completionBlock(0,nil,response.result.error?.localizedDescription ?? "Some thing went wrong", statusCode)
                    
                }
            case .failure(let error):
                completionBlock(0,nil,error.localizedDescription, statusCode)
            }
        }
    }
    
    
    func TableRoomListWebService(completionBlock:@escaping (_ success: Int, _ data : TableRoomParser?, _ message: String) -> Void) {
        if checkAuthToken() == false {
            return
        }
        
        let auth = "Bearer " + strToken
        print(auth)
        let header = ["Accept": "application/json","Authorization": auth]
        let url = "http://admin.vrsites.co/api/v1/locations/index"
       
        Alamofire.request(url,method: .get ,headers: header).responseJSON { response in
        
        getStatusCode = 0
        let statusCode = (response.response?.statusCode) ?? 0
        print(statusCode)
        getStatusCode = statusCode
            
            switch response.result {
            case.success:
                if let value = response.result.value {
                    let json = JSON(value)
                  
                    print("=========Tabel Room Data json is:\n\(json)")
                    let parser = TableRoomParser(json: json)
                  
                    completionBlock(parser.result,parser,parser.responseMessage)
                }else{
                    completionBlock(0,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    
                }
            case .failure(let error):
                completionBlock(0,nil,error.localizedDescription)
            }
        }
    }
}
